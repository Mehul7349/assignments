max(A,B,C,D,M):-
	(A>B->X=A;X=B),
	(C>D->Y=C;Y=D),
	(X>Y->M=X;M=Y).

max2(A,B,C,D,M):-(B=0),C=0,D=0,M=A.
max2(A,B,C,D,M):-(A>B)->max2(A,C,D,0,M);max2(B,C,D,0,M).